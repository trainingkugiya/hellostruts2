package jp.co.sakusaku.training.hellostruts.action;

import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.sakusaku.training.hellostruts.actionform.HelloForm;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class HelloAction extends Action {

@Override
public ActionForward execute(ActionMapping mapping, ActionForm form,
HttpServletRequest req, HttpServletResponse resp) throws Exception {

HelloForm helloForm = (HelloForm) form;
Date now = new Date();
helloForm.setNow(now);

Calendar calendar = Calendar . getInstance();

int nowHour = calendar.get(calendar.HOUR_OF_DAY);
String message = "";

if(nowHour >= 6 && nowHour < 12) {
 message = "Good Morinig Struts";
}
else if(nowHour >= 12 && nowHour < 18) {
 message = "Good afternoon Struts";
}
else if(nowHour >= 18 && nowHour < 24){
message = "Good evening Strus";
}
else if(nowHour >= 0 && nowHour < 6) {

 message = "Good evening Strus";
}

helloForm.setMessage(message);
req.setAttribute("form", helloForm);

return mapping.findForward("success");
}
}