package jp.co.sakusaku.training.hellostruts.actionform;

 import java.util.Date;

import org.apache.struts.action.ActionForm;

 public class HelloForm extends ActionForm {

 private Date now;

 private String message;

 public Date getNow() {
 return now;
 }
 public void setNow(Date now) {
 this.now = now;
 }

 public String getMessage(){
	 return message;
 }
 public void setMessage(String message){
	 this.message = message;

 }


 }